/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * 
 * Generated with the TypeScript template
 * https://github.com/emin93/react-native-template-typescript
 * 
 * @format
 */

import React, { Component, createContext } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import { Router, Stack, Scene, Drawer } from "react-native-router-flux"
import Icon from "react-native-vector-icons/FontAwesome";

//screens
import Home from "./screens/Home";

import DrawerContent from "./components/DrawerContent";


//context
import store, { IinitiailState } from "./context/store"
import actions from "./context/actions"
import context from "./context";
const { Provider } = context;

interface Props { }

export default class App extends Component<Props, IinitiailState> {

  constructor(props: Props) {
    super(props);

    this.state = store;

  }

  render() {
    return (
      <Provider value={{
        store: this.state,
        actions: actions(this),
      }}>
        <Router>
          <Stack key="root">

            <Drawer
              hideNavBar
              key="drawer"
              contentComponent={DrawerContent}
              drawerIcon={() => <Icon name="home" />}
            >
              <Scene key="home" component={Home} title="Home" />
            </Drawer>
          </Stack>
        </Router>
      </Provider>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});