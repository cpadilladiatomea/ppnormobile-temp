import React, { createContext } from "react";
import actions, { Actions } from "./actions";
import store, { IinitiailState } from "./store"
import App from "../app";

interface Context {
  store: IinitiailState,
  actions: Actions
}


const context = createContext<Context>({
  store,
  actions: actions({} as App)
})

export default context;