import App from "../../app";

export interface Ihome {
  sayHello: Function
}

export default (self: App): Ihome => ({
  sayHello: () => {
    self.setState({ home: { hola: "hola" } })
  }
})