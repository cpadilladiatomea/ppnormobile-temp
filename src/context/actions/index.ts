import App from "../../app";
import home, { Ihome } from "./home";


export interface Actions {
  home: Ihome
}


export default (self: App): Actions => ({
  home: home(self)
})