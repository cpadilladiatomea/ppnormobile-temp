/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * 
 * Generated with the TypeScript template
 * https://github.com/emin93/react-native-template-typescript
 * 
 * @format
 */

import React, { Component, useContext } from 'react';
import { Platform, StyleSheet, Text, View, TouchableOpacity } from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';

import context from "../context";

const { Consumer } = context;
interface Props { }
export default class Home extends Component<Props> {


  render() {
    return (
      <Consumer>
        {
          ({ store, actions }) =>
            <View style={styles.container}>
              <Icon name="home" size={40} />
              <TouchableOpacity onPress={() => actions.home.sayHello()}>
                <Text style={styles.welcome}>home,{store.home.hola}</Text>
              </TouchableOpacity>
            </View>
        }
      </Consumer>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});